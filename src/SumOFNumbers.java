/**
 * Created by Dmitry on 7/15/2015.
 */
import java.io.IOException;
import java.util.Scanner;

public class SumOFNumbers {
    public static void main(String[] args) throws IOException{
        Scanner keyboard = new Scanner(System.in);
        String input;
        System.out.print("Enter series of numbers separated by coma without spaces: ");
        input = keyboard.nextLine();

        String[] numbersStrArr = input.split("\\,");
        int[] arrayInt = new int[numbersStrArr.length];
        int total = 0;

        try {
            for (int n = 0; n < numbersStrArr.length; n++) {
                arrayInt[n] = Integer.parseInt(numbersStrArr[n]);
                total = total + arrayInt[n];
                System.out.print("\nSum: " + total);
            }
        } catch (NumberFormatException nfe) {
            System.out.print("\nERROR: Wrong input");
        }
        keyboard.close();
    }
}
